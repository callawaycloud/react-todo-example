import React from 'react';
import { connect } from 'react-redux';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store'
import { Todo }  from '../../../modules/todo/todo';
//import the actions
import * as todoActions from '../../../actions/todo'
import * as doneActions from  '../../../actions/done'

const mockStore = configureMockStore([]);

it('renders without crashing while empty', () => {
    const store = mockStore({todo: []})
    let todo = shallow(<Todo todos={[]}/>)
    expect(todo.find('.todolist').exists()).toEqual(true)
})

it('renders without crashing with list', () => {
   let todo = shallow(<Todo todos={['one','two']} />)
   expect(todo.find('.todolist').exists()).toEqual(true)
   expect(todo.find('ul').children().length).toEqual(2)
   expect(todo.find('.count-todos').text()).toEqual('2')
})

it('renders with a redux state', () => {
    const store = mockStore({todo: ['one','two']})
    let todo = shallow(<Todo todos={store.getState().todo} dispatch={store.dispatch} />)
    expect(todo.find('.todolist').exists()).toEqual(true)
    expect(todo.find('ul').children().length).toEqual(2)
    expect(todo.find('.count-todos').text()).toEqual('2')
})

it('mark as done dispatches actions', () => {
    const store = mockStore({todo: ['one','two']})
    let todo = shallow(<Todo todos={store.getState().todo} dispatch={store.dispatch} />)
    expect(todo.find('.todolist').exists()).toEqual(true)
    expect(todo.find('ul').children().length).toEqual(2)
    //find the todoItem and run the click fucntion
    let first = todo.find('ul').children().first()
    expect(first.props().text).toEqual('one')
    //expected action value
    let todoAction = {
        type: todoActions.REMOVE_TODO,
        payload: 0
    }
    let doneAction = {
        type: doneActions.ADD_DONE,
        payload:first.props().text
    }
    //fire click action
    first.props().click()
    expect(store.getActions()).toEqual([todoAction, doneAction])
    
})
