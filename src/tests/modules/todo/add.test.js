import React from 'react';
import { connect } from 'react-redux';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store'
import { Add }  from '../../../modules/todo/add';
//import the actions
import * as  actions  from '../../../actions/todo'

const mockStore = configureMockStore([]);

it('renders without crashing', () => {
    const store = mockStore({todo: []})
    let add = shallow(<Add dispatch={store.dispatch} />)
    expect(add.find('input').exists()).toEqual(true)
})

it('should handle event', () => {
    const store = mockStore({todo: [] })
    let add = shallow(<Add dispatch={store.dispatch} />)
    expect(store.getState().todo).toEqual([])
    //add in some text
    add.simulate('keyDown', {keyCode: 13,target: {value: 'hello'}})
    //expected action value
    let action = {
        type: actions.ADD_TODO,
        payload: 'hello'
    }
    expect(store.getActions()).toEqual([action])
})
