import { combineReducers } from 'redux'
//Import reducers
import todos from './todo'
import done from './done'

export default combineReducers({
    todos,
    done
})