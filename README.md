Sample ToDo Application

# Reqires
- Latest Node Package Manager (npm)

# Installation
- Clone repo
- run `$ npm install`


# To Run Locally

`
$ npm start
`

# To run Unit Tests

`
$ npm test 
`
