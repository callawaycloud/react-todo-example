import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../../actions/todo'

export class Add extends Component {
    handleChange(event){
        if(event.keyCode === 13){
            if(event.target.value){
                this.props.dispatch(addTodo(event.target.value))
            }
            event.target.value = ''
        }
    }
    
    render(){
        return(
            <input type="text" className="form-control add-todo" placeholder="Add todo" onKeyDown={this.handleChange.bind(this)} />
        )
    }
}
//auto add the connet app
export default connect()(Add)