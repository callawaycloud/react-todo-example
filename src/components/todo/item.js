import React, { Component } from 'react'

class TodoItem extends Component {
    static propTypes = {
        text: React.PropTypes.string.isRequired,
        click: React.PropTypes.func
    }
    render() {
        return (
            <li>
                <button onClick={this.props.click} className='btn btn-default btn-xs pull-left'><span className="glyphicon glyphicon-remove"></span></button>  {this.props.text}
            </li>
        )
    }
}

export default TodoItem



