export const ADD_DONE = 'ADD_DONE'
export const REMOVE_DONE = 'REMOVE_DONE'

export const addDone = (done) => {
    return {
        type: ADD_DONE,
        payload: done
    }
}
//takes the index and removes it
export const removeDone = (index) => {
    return {
        type: REMOVE_DONE,
        payload: index
    }
}