import React from 'react';
import { connect } from 'react-redux';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store'
import { Done }  from '../../../modules/done/done';
//import the actions
import * as doneActions from  '../../../actions/done'

const mockStore = configureMockStore([]);

it('renders without crashing while empty', () => {
    const store = mockStore({done: []})
    let done = shallow(<Done />)
    expect(done.find('#done-items').exists()).toEqual(true)
})

it('renders without crashing with list', () => {
   let done = shallow(<Done done={['one','two']} />)
   expect(done.find('#done-items').exists()).toEqual(true)
   expect(done.find('ul').children().length).toEqual(2)
})

it('renders with a redux state', () => {
    const store = mockStore({done: ['one','two']})
    let done = shallow(<Done done={store.getState().done} dispatch={store.dispatch} />)
    expect(done.find('#done-items').exists()).toEqual(true)
    expect(done.find('ul').children().length).toEqual(2)
})

it('mark as done dispatches actions', () => {
    const store = mockStore({done: ['one','two']})
    let done = shallow(<Done done={store.getState().done} dispatch={store.dispatch} />)
    expect(done.find('#done-items').exists()).toEqual(true)
    expect(done.find('ul').children().length).toEqual(2)
    //find the doneItem and run the click fucntion
    let first = done.find('ul').children().first()
    expect(first.text()).toEqual('one')
    //expected action value
    let doneAction = {
        type: doneActions.REMOVE_DONE,
        payload: 0
    }
    //fire click action
    first.find('button').simulate('click')
    expect(store.getActions()).toEqual([doneAction])
    
})
