import React, { Component } from 'react'
import { connect } from 'react-redux'
import Add from './add.js'
import TodoItem from '../../components/todo/item.js'
//import actions
import { removeTodo } from '../../actions/todo'
import { addDone } from '../../actions/done'

export class Todo extends Component {
    
    renderTodos(){
        if(this.props.todos){
            let self = this
            return this.props.todos.map((t, i) => {
                return <TodoItem text={t} key={i} click={self.markAsDone.bind(self, i)} />
            })
        }
    }
    markAsDone(index){
        let todo = this.props.todos[index]
        this.props.dispatch(removeTodo(index))
        this.props.dispatch(addDone(todo))
    }
    render() {
        return (<div className="col-md-6">
            <div className="todolist not-done">
                <h1>Todos</h1>
                <Add />
                <hr />
                <div>
                    <ul id="sortable" className="list-unstyled">
                        {this.renderTodos()}
                    </ul>
                </div>
                <div className="todo-footer">
                    <strong><span className="count-todos">{this.props.todos.length}</span></strong> Items Left
                 </div>
            </div>
        </div>)
    }
}
export default connect((state) => {
  return { todos: state.todos }
})(Todo)