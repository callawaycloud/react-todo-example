//import todo Action constants
import * as actions from '../actions/done'

const initState = []

const done = (state = initState, action = {}) => {
    switch(action.type){
        case actions.ADD_DONE:
            //why not array push? because each state has to be a completely new object
            //array push modifiys the current object (state) instead of generating a new one
            //this is shorthand for state.concat([action.payload])
            return [...state, action.payload]
        case actions.REMOVE_DONE:
            //why not some slices here? 
            //array slice modifices the current object (state) instead of generating a new one
            //this one takes the sate, splits it into two arrays based on the index given
            //and then concats them, which returns a brand new array object
            return [...state.slice(0, action.payload), ...state.slice(action.payload + 1)]
        default:
            return state       
    }
}

export default done