import React, { Component } from 'react'
import { connect } from 'react-redux'
import { removeDone } from '../../actions/done'

export class Done extends Component {

    renderDone(){
        const self = this
        if(this.props.done){
            return this.props.done.map((d, i) => {
                return (
                    <li key={i}>{d}<button className="remove-item btn btn-default btn-xs pull-right" onClick={self.removeFromDone.bind(self, i)}><span className="glyphicon glyphicon-remove"></span></button></li>
                )
            })
        }
    }
    removeFromDone(index){
        this.props.dispatch(removeDone(index))
    }
    render() {
        return (
            <div className="col-md-6">
                <div className="todolist">
                    <h1>Already Done</h1>
                    <ul id="done-items" className="list-unstyled">
                        {this.renderDone()}
                    </ul>
                </div>
            </div>)
    }
}

export default connect((state) => {
   return { done: state.done }
})(Done)